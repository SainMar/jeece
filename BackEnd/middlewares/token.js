//Token d'authentification
const jwt = require('jsonwebtoken');

//Comparaison des token d'authentification
module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization;
    const decodedToken = jwt.verify(token, '07a1c869f7bc7b20c2058ebabf8a0ad9'); //Comparaison des tokens
    const userId = decodedToken.userId;
    //Indique que le token ne correspond pas au User
    if (req.body.userId && req.body.userId !== userId) {
      throw 'User ID non valable !';
    } else {
      next();
    }
  } catch (error) {
    res.status(401).json({
      error: new Error('Requete invalide !')
    });
  }
};
