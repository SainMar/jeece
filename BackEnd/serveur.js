//Serveur

//Librairies
const http = require('http');
const path = require('path');
//const socketio = require('socket.io');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const ent = require('ent');
const morgan = require('morgan');

//Librairie qui contient les routes d'accès aux différentes fonctionnalités
const routes = require('./routes/routes.js');

//Framework express
const app = express();
//Création du serveur
const server = http.createServer(app);
//Connexion socketio
const io = require('socket.io').listen(server);


///MORGAN PR TRACER
//app.use(morgan('tiny'))

//Charge tous les fichiers statiques
app.use(express.static(__dirname));

//Connexion à la base de données
mongoose.connect('mongodb+srv://Ahaz1701:Ahaz1701@testjeece.a1tcc.mongodb.net/TestJEECE?retryWrites=true&w=majority',
  { useNewUrlParser: true,
    useUnifiedTopology: true,
    autoIndex: false })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));

//Permission FrontEnd (cross-origin)
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
})


//Tableau des users connectés
var users = [];

//Lors de la connexion d'un user, on stocke dans ses variables de session, son pseudo et son id
io.on("connection", socket => {
  
  //console.log(socket.id)
    //socket.emit("connect", users);
    socket.on('USER_CONNECT', data => {
       console.log(data);
       users.push({id: data.id, status: true});
       console.log(users);
       socket.broadcast.emit('CONNECT', users);
    });
    socket.on('MSG_ENVOYER', data => {
      
      console.log(socket)
      //io.emit('MSG_RECU', data)
    });
});

//On parse les requêtes au format json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//Routes d'accès aux divers fonctionnalités
app.use('/JEECE', routes);



//On écoute sur le port 7010
const PORT = 7010 || process.env.PORT;
server.listen(PORT, () => console.log(`Serveur running on port ${PORT}`));


//Tableau des users connectés
var users = [];

//Lors de la connexion d'un user, on stocke dans ses variables de session, son pseudo et son id
/*io.on("connection", socket => {
  //console.log(socket.id)
    //socket.emit("connect", users);
    socket.on('USER_CONNECT', data => {
       console.log(data);
       users.push({id: data.id, status: true});
       console.log(users);
       socket.broadcast.emit('CONNECT', users);
    });
    socket.on('MSG_ENVOYER', data => {
      
      console.log(socket)
      //io.emit('MSG_RECU', data)
    });
});*/
