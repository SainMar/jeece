import axios from 'axios';


export const HTTP = axios.create({
  baseURL: `http://localhost:7010/JEECE/`,
  timeout: 10000,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Accept' : 'application/json, text/plain, */*',
    'Access-Control-Allow-Methods' : 'GET, PUT, POST, DELETE, OPTIONS',
    'Access-Control-Allow-Credentials' : true,
    'Content-Type': 'application/json',
  }
})