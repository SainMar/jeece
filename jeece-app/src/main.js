import Vue from 'vue'
//import Vuex from 'vuex'
import App from './App.vue'
import router from './router'
import store from './store.js'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import moment from 'moment'

//import VueSocketIO from "vue-socket.io";


//Vue.use(VueSocketIO, 'http://localhost:7010' )



Vue.config.productionTip = false;



///MOMENT
Vue.use(moment)

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

//Vue.use(Vuex)


new Vue({ 
  router,
  store,
  render: h => h(App),
}).$mount('#app')
