import Vue from 'vue'
import VueRouter from 'vue-router'
import Inscription from '../views/Inscription.vue'
import Login from '../views/Login.vue'
import Accueil from '../views/Accueil.vue'
import SalleDiscussion from '../views/SalleDiscussion.vue'
import Logout from '@/components/Logout.vue'
import store from '../store.js'

Vue.use(VueRouter)

const routes = [
  
  {
    path: '/',
    name: 'Accueil',
    component: Accueil,
    meta: {
      requiresAuth : true,
    }
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login,
    meta: {
      requiresVisitor : true,
    }
  },
  {
    path: '/Inscription',
    name: 'Inscription',
    component: Inscription,
    meta: {
      requiresVisitor : true,
    }
  }, 
  {
    path: '/SalleDiscussion',
    name: 'SalleDiscussion',
    component: SalleDiscussion,
    meta: {
      requiresAuth : true,
    }
  },
  {
    path: '/Logout',
    name: 'Logout',
    component: Logout
  },

]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.getters.loggedIn) {
      next({
        path: '/Login',
      })
    } else {
      next()
    }
  } 
  else{
    next() // make sure to always call next()!
  } 
})




export default router
