import Vue from 'vue';
import Vuex from 'vuex';
import router from './router';



Vue.use(Vuex);


const store = new Vuex.Store({

    //Contient toutes les datas accessible par chaque composant du frontend
    state: {
        //INSCRIPTION
        msgInscription : "en attente d inscription",
        token: localStorage.getItem('token'),
        userId: localStorage.getItem('userId'),

        


        //INFO USER
        infoUser :{
            prenom:'',
            nom:'',
            email:'', 
            urlPhoto:''
        },
        infoOtherUser :{
            prenom:'',
            nom:'',
            email:'', 
            urlPhoto:''
        },
        //ROOM
        listRoom:{

        },
        idRoomSelected: localStorage.getItem('idRoomSelected'),
        otherUserId: localStorage.getItem('otherUserId'),

        //USERS CO ET DECO
        listUser:{

        },

        //SALLE DISCUSSION
        listMsg:{

        },


    },

    //Permet à tous les composants du frontend de getter(récupérer) de la data stockée dans le state
    getters: {
        getMsgInscription(state){
            return state.msgInscription;
        },
        getToken(state){
            return state.token;
        },
        getIdUser(state){
            return state.userId;
        },
        getInfoUser(state){
            return state.infoUser;
        },
        getListRoom(state){
            return state.listRoom;
        },
        getListUser(state){
            return state.listUser;
        },
        getListMsg(state){
            return state.listMsg;
        },
        getIdRoomSelected(state){
            return state.idRoomSelected;
        },
        getIdOtherUser(state){
            return state.otherUserId;
        },
        loggedIn(state){
            return  state.token!= null;
        }
    },

    //Permet de modifier la data dans le state
    mutations: {
        //Ne pas oublié on commit en faisant par exemple store.commit(`nom de la focntion`, value);
        // Cette option se fait dans les actions la plus part du temps 
        
        //LOGIN
        SET_TOKEN_AND_ID(state, post){
            state.token = post.token;
            state.userId = post.userId;
        },


        //INSCRIPTION 
        SET_MSG_INSCRIPTION(state, post){
            state.msgInscription = post
        },

        //ACCUEIL
        SET_INFO_USER(state, post){
            state.infoUser.prenom=post.prenom;
            state.infoUser.nom=post.nom;
            state.infoUser.email=post.email;
            state.infoUser.urlPhoto=post.urlPhoto;
        },

        //ROOM
        SET_LIST_ROOM(state, post){
            state.listRoom = post;
        },
       /* SET_ID_ROOM_SELECTED(post){
            //state.idRoomSelected = post;
           
        },
        SET_OTHER_USER_ID(state, post){
            state.otherUserId = post;
        },*/

        //USER
        SET_LIST_USER(state, post){
            state.listUser = post;
        },

        //SALLE DISCUSSION
        SET_LIST_MSG(state, post){
            state.listMsg = post;
        },

        //LOGOUT
        RESET(state){
            state.token = null;
            state.userId = null;
        },

        
        

    },

    //Le plus souvent il s'agit de requete vers l'API du backend on va appelé le mutation pour commit les changements de state qui serviront à charger toutes les sections data des composants
    actions: {
        //INSCRIPTION 
               async inscription(context, post){
                    //Method pour connecter avec DB
                    //On attend la réponse de l'api pour renvoyer un message

                    try{
                        // POST request using fetch with async/await
                        const requestOptions = {
                            method: "POST",
                            headers: { 
                                    'Accept' : 'application/json',
                                    'Content-Type': 'application/json'
                             },
                            body: JSON.stringify(post)
                        };
                        await fetch("http://localhost:7010/JEECE/auth/signup", requestOptions)
                        .then( async response => {
                            const data = await response.json();
                            //console.log(data.message)
                            alert(data.message)
                            router.push({name: 'Login'})
                        })
                        .catch(err => {
                            console.log('Voici l\'err retrouvée (INSCRIPTION):  ' +err)
                        });
                        
                    }catch{
                        return {error: 'Pb lors de linscription'}
                    }
                    

                        
                },
                verifInscription(context, post){
                    context.commit('SET_MSG_INSCRIPTION', post)
                },
        //LOGIN
                
                async login(context, post){
                    //Method pour connecter avec DB
                    //On attend la réponse de l'api pour renvoyer un message
                    //console.log('login starting...')
                    try{
                        // POST request using fetch with async/await
                        const requestOptions = {
                            method: "POST",
                            headers: { 
                                    'Accept' : 'application/json',
                                    'Content-Type': 'application/json'
                            },
                            body: JSON.stringify(post)
                        };
                        await fetch("http://localhost:7010/JEECE/auth/login", requestOptions)
                        .then( async  response => {
                            const data =  await response.json();
                            //console.log(data)
                            
                            var post = {token : data.token, userId: data.userId};
                            context.commit('SET_TOKEN_AND_ID', post)

                            localStorage.setItem('token', data.token)
                            localStorage.setItem('userId', data.userId)
                            
                            
                            
                            router.push({name: 'Accueil'})
                              
                                
                                //this.$forceUpdate()
                            
                            
                        })
                        .catch(err => {
                            console.log('Voici l\'err retrouvée (LOGIN):  ' +err)
                        });
                        //console.log('login finished')
                        
                    }catch{
                        return {error: 'Pb lors de login'}
                    }
                    

                        
                },
        
        //CHARGEMENT INFORMATION USER CONNECTE
                async fetchInfoUser(context){
                    var post ={
                        userId: localStorage.getItem('userId')
                    }
                    
                    //console.log(post)
                    try{
                        // POST request using fetch with async/await
                        const requestOptions = {
                            method: "POST",
                            headers: { 
                                    'Accept' : 'application/json',
                                    'Content-Type': 'application/json',
                                    'authorization': this.state.token,
                            },
                            body: JSON.stringify(post)
                        };
                        await fetch("http://localhost:7010/JEECE/user/findUser", requestOptions)
                        .then( async response => {
                            const data = await response.json();
                            context.commit('SET_INFO_USER', data)
                        })
                        .catch(err => {
                            console.log('Voici l\'err retrouvée (CHARGEMENT INFO USER):  ' +err)
                        });

                    }catch{
                        return {error: 'Pb lors du chargment des informations de l\'utilisateur connecté'}
                    }
                },
        //CHARGEMENT DES ROOMS DEJA EXISTANTES
                async fetchListRoom(context){
                    var post ={
                        userId: this.state.userId
                    }
                    try{

                        // POST request using fetch with async/await
                        const requestOptions = {
                            method: "POST",
                            headers: { 
                                    'Accept' : 'application/json',
                                    'Content-Type': 'application/json',
                                    'authorization': this.state.token,
                            },
                            body: JSON.stringify(post)
                        }; 
                        await fetch("http://localhost:7010/JEECE/messagerie/accueuil", requestOptions)
                        .then( async response => {
                            const data = await response.json();
                        
                            context.commit('SET_LIST_ROOM', data)

                        })
                        .catch(err => {
                            console.log('Voici l\'err retrouvée (CHARGEMENT LIST ROOM):  ' +err)
                        });

                    }catch{
                        return {error: 'Pb lors du chargement de la liste des rooms déjà existantes'}
                    }
                },
                async setIdRoomSelected(context, post){
                    try {
                         //await context.commit('SET_ID_ROOM_SELECTED', post.idRoom);
                        localStorage.setItem('idRoomSelected', post.idRoom);
                         router.push({name: 'SalleDiscussion'})
                    }catch{
                        return {error: 'Pb lors de la selection de la room que lutilisateur veut ouvrir'}
                    }
                },
                async setOtherUserId(context, post){
                    try {
                         //await context.commit('SET_ID_ROOM_SELECTED', post.idRoom);
                        localStorage.setItem('otherUserId', post.otherUserId);
                        //context.commit('SET_OTHER_USER_ID', post.otherUserId);
                        //console.log(this.state.otherUserId)
                    }catch{
                        return {error: 'PB LORS DE L ENREGISTREMENT DE L ID DE L AUTRE USER'}
                    }
                },
        //CHARGEMENT LIST DES USERS 
                async fetchListUser(context){
                    try{
                        // POST request using fetch with async/await
                        const requestOptions = {
                            method: "GET",
                            headers: { 
                                    'Accept' : 'application/json',
                                    'Content-Type': 'application/json',
                                    'authorization': this.state.token,
                            },
                        }; 
                        await fetch("http://localhost:7010/JEECE/user/listUser", requestOptions)
                        .then( async response => {
                            const data = await response.json();
                            
                            //console.log(data.listUser[1].date)

                            var list=[];
                            for(var i=0; i< data.listUser.length ; i++){
                                var post = {
                                    prenom:data.listUser[i].prenom,
                                    nom:data.listUser[i].nom,
                                    urlPhoto:data.listUser[i].urlPhoto,
                                    _id:data.listUser[i]._id,
                                    status: false
                                }
                                list.push(post)
                            }
                            

                            context.commit('SET_LIST_USER', list)
                        })
                        .catch(err => {
                            console.log('Voici l\'err retrouvée (CHARGEMENT LIST USER):  ' +err)
                        });

                    }catch{
                        return {error: 'Pb lors du chargement de la liste des users déjà existantes'}
                    }
                },
                testExistRoom(context, post){
                    return new Promise((resolve, reject) => {
                        var idRoomToTest1 = this.state.userId+post;
                        var idRoomToTest2 = post+this.state.userId;
                        var list= this.state.listRoom;
                        for(var i=0; i< list.length ; i++){
                           
                                if(list[i]._id == idRoomToTest1){
                                    
                                    resolve(idRoomToTest1)
                                }
                                if(list[i]._id == idRoomToTest2){
                                    
                                    resolve(idRoomToTest2)
                                }  
                        }
                        reject(post)
                    })
                },
        //CHARGEMENT LIST DES MESSAGE
                async fetchListMsg(context){
                    try{
                        // POST request using fetch with async/await
                        const requestOptions = {
                            method: "GET",
                            headers: { 
                                    'Accept' : 'application/json',
                                    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                                    'authorization': this.state.token,
                            },
                            
                        }; 
                        var premierPartie = "http://localhost:7010/JEECE/messagerie/";
                        var url = premierPartie.concat(this.state.idRoomSelected);
                        await fetch(url, requestOptions)
                        .then( async response => {
                            const data = await response.json();
                            
                            context.commit('SET_LIST_MSG', data)
                            console.log(data)
                            
                        })
                        .catch(err => {
                            console.log('Voici l\'err retrouvée (CHARGEMENT LIST USER):  ' +err)
                        });

                    }catch{
                        return {error: 'Pb lors du chargement de la liste des users déjà existantes'}
                    }
                },
                async postNewMsg(context, post){
                    try{
                        // POST request using fetch with async/await
                        const requestOptions = {
                            method: "POST",
                            headers: { 
                                    'Accept' : 'application/json',
                                    'Content-Type': 'application/json',
                                    'authorization': this.state.token,
                            },
                            body: JSON.stringify(post)
                        };
                        var premierPartie = "http://localhost:7010/JEECE/messagerie/";
                        var url = premierPartie.concat(this.state.idRoomSelected);
                        await fetch(url, requestOptions)
                        .then( async response => {
                            const data = await response.json();
                            console.log(data)
                            
                        })
                        .catch(err => {
                            console.log('Voici l\'err retrouvée (SEND MSG):  ' +err)
                        });
                        
                    }catch{
                        return {error: 'Pb lors de l envoie de msg'}
                    }
                },
        ///LOGOUT
                async destroySession(context){
                    if(context.getters.loggedIn){
                        localStorage.clear();
                        context.commit('RESET')
                    
                    }
                }                

    },


});

export default store